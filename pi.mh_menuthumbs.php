<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include composer autoload
require 'vendor/autoload.php';

// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;
	
$plugin_info = array(
    'pi_name'         => 'Menu thumbnail generator voor Cafe de Haven',
    'pi_version'      => '1.0',
    'pi_author'       => 'MH Webcreative',
    'pi_author_url'   => 'http://www.maikhoogkamer.nl/',
    'pi_description'  => 'Genereren menutegels voor Sunday Roast van Cafe de Haven',
    'pi_usage'        => ''
);

class Mh_menuthumbs {
	
    public $return_data = "";
    
    private $text;
    private $color;
    private $text_size;
    private $text_posx;
    private $text_posy;
    private $text_halign;
    private $text_valign;
    private $save_as_file;
    
    protected $img;
    
    private $base_img;
    private $title;
    private $text1;
    private $text2;
    private $price;
    
    public function __construct() {
	    
	
        $this->base_img 	= ee()->TMPL->fetch_param('base_img');
        
        $this->title 		= strtoupper(ee()->TMPL->fetch_param('title'));
        $this->text1 		= ee()->TMPL->fetch_param('text1') ? ee()->TMPL->fetch_param('text1'): null;
        $this->price 	    = ee()->TMPL->fetch_param('price') ? ee()->TMPL->fetch_param('price'): null;
        
		$this->save_as_file	= ee()->TMPL->fetch_param('save') ? ee()->TMPL->fetch_param('save') : false;

		if(!isset($this->base_img)) {
			echo 'Geen achtergrondafbeelding geselecteerd.'; 
			return false;
		}
		
		if(!isset($this->title)) return false;

		// create an image manager instance with favored driver
		Image::configure(array('driver' => 'imagick'));
		
		$this->img =& Image::make($this->base_img);
		
		if(!isset($this->title)) return false;

		$titleLinesEnd = $this->placeMenuTitle();
		$menuLinesEnd = $this->placeMenuText($titleLinesEnd);
		
		$this->placeMenuPrice($menuLinesEnd);
		
		$this->generateImage();


    }
    
    private function placeMenuTitle() {
	    
	    if(isset($this->title) && !empty($this->title)) {
		    
		    $title = wordwrap($this->title, 14, "|");
		    $titleLines = explode("|", $title);
		    $titleLines = array_filter($titleLines);

		    $titleStartX = 440;
		    $lineCounter = 0;
		    
		    foreach($titleLines as $line):
			    
		   		$this->img->text($titleLines[$lineCounter], 500, ($titleStartX + (70 * $lineCounter)), 
					function($font) {
					    $font->file(PATH_THIRD . basename(__DIR__) . '/fonts/franklin-gothic/ITCFranklinGothicStd-Demi.ttf');
					    $font->size(70);
					    $font->color('#000');
					    $font->align('center');
					    $font->valign('top');
					    $font->kerning('6');
					}
				);
				
			    $lineCounter ++;
			    
			endforeach;
		
			return ($titleStartX + (70 * $lineCounter));
			
		}
		
    }
    
    private function placeMenuText($menuLineStart = 540) {
	    
	    if(isset($this->text1) && !empty($this->text1)) {

		    $descr = wordwrap($this->text1, 40, "|");
		    $descrLines = explode("|", $descr);
		    $descrLines = array_filter($descrLines);
	
		    $descrStartX = $menuLineStart + 10;
		    $lineCounter = 0;
		    
		    foreach($descrLines as $line):
		   		
		   		$this->img->text($descrLines[$lineCounter], 500, ($descrStartX + (39 * $lineCounter)), 
					function($font) {
					    $font->file(PATH_THIRD . basename(__DIR__) . '/fonts/boton/boton-regular-webfont.ttf');
					    $font->size(34);
					    $font->color('#333');
					    $font->align('center');
					    $font->valign('top');
					}
				);
				
				$lineCounter++;
			endforeach;		
			
			return ($descrStartX + (39 * $lineCounter));    
	    }
    }
    
    private function placeMenuPrice($LineStart = 694) {
	    
	    if(isset($this->price) && !empty($this->price)) {
		    
	   		$this->img->text('—', 500, $LineStart, 
				function($font) {
				    $font->file(PATH_THIRD . basename(__DIR__) . '/fonts/franklin-gothic/ITCFranklinGothicStd-Demi.ttf');
				    $font->size(40);
				    $font->color('#8b8880');
				    $font->align('center');
				    $font->valign('top');
				}
			);
		    
	   		$this->img->text($this->price, 500, ($LineStart + 40), 
				function($font) {
				    $font->file(PATH_THIRD . basename(__DIR__) . '/fonts/franklin-gothic/ITCFranklinGothicStd-Demi.ttf');
				    $font->size(30);
				    $font->color('#333');
				    $font->align('center');
				    $font->valign('top');
				    $font->kerning('2');
				}
			);
		    
	    }
	    
    }
    
    private function generateImage() {
	    
	    //Generate thumbnail of 1000x1000 pixels
	    $this->img->resize(1000, null, function ($constraint) {
            $constraint->aspectRatio();
        });

		if($this->save_as_file == true) {
			
			$filename = 'sundayroast_' . date('dmY-his'). '.png';
			$this->img->save(PATH_THIRD . basename(__DIR__) . '/made/' . $filename);
			
			$this->return_data = '<p>File saved as <a href="'.PATH_THIRD . basename(__DIR__) . '/made/' . $filename . '">' . $filename . '</a></p>';
			
		} else {
			
			header('Content-Type: image/png');
			echo $this->img->encode('png');
		}
    }
    
    // --------------------------------------------------------------------

    /**
     * Usage
     *
     * This function describes how the plugin is used.
     *
     * @access  public
     * @return  string
     */
    public static function usage()
    {
        ob_start();  ?>

		Manipulatie van afbeeldingen
    <?php
        $buffer = ob_get_contents();
        ob_end_clean();

        return $buffer;
    }
    // END
}